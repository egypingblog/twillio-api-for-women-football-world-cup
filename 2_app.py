import os
from flask import Flask, request
import requests
from twilio.twiml.messaging_response import MessagingResponse


app = Flask(__name__)



countries = ['KOR', 'PAN', 'MEX', 'ENG', 'COL', 'JPN', 'POL', 'SEN',
'RUS', 'EGY', 'POR', 'MAR', 'URU', 'KSA', 'IRN', 'ESP',
'DEN', 'AUS', 'FRA', 'PER', 'ARG', 'CRO', 'BRA', 'CRC',
'NGA', 'ISL', 'SRB', 'SUI', 'BEL', 'TUN', 'GER', 'SWE']

urls = {'group': 'https://worldcup.sfg.io/teams/group_results',
'country': 'http://worldcup.sfg.io/matches/country?fifa_code=',
}


@app.route('/', methods=['POST'])
def receive_sms():
  body = request.values.get('Body', None)
  resp = MessagingResponse()

  if body.upper() in countries:
        html = requests.get(urls['country']+body).json()
        headline = "\n--- Past Matches ---\n"
        for match in html:
              if match['status'] == 'completed':
                    headline += (
                      match['home_team']['country'] + " " +
                      str(match['home_team']['goals']) + " vs " +
                      match['away_team']['country'] + " " +
                      str(match['away_team']['goals']) + "\n"
                      )
  elif body == 'list':
    headline = '\n'.join(countries)

  resp.message(headline)
  return str(resp)


if __name__ == "__main__":
  port = int(os.environ.get("PORT", 5000))
  app.run(host='0.0.0.0', port=port)